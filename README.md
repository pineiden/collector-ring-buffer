Este módulo entrega un objeto de tipo ringbuffer para
controlar una ventana de tiempo de un stream de datos en el parámetro de la latencia, es decir la diferencia de tiempo en que se genera el dato y se recibe.


Se instala en modo de desarrollo


```shell
pip install -e .
```

En modo producción

```shell
pip install .
```

Para correr los test.

```
python3 -m unittest discover tests
```
